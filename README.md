# Userbot is user-friendly Telegram Bot

Бот с интуитивно-понятным интерфейсом и минимальным функционалом.

## Запуск

**Важно** добавить токен Бота в `src/.env` по аналогии с `.env.example`.

### В контейнерах
```shell
docker-compose up -d --build
```

### Либо вручную:

Venv и миграции
```shell
pipenv install --ignore-pipfile
```

Запуск сервера
```shell
python src/manage.py runserver
```

Запуск бота
```shell
python src/manage.py bot
```

Для удобства также конфиги в .run для запуска через PyCharm.

---

P.S.
Makefile взят из шаблона, мб понадобится для запуска. Я на винде, к сожалению :(

