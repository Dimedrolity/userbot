from django.contrib.auth.models import AbstractUser
from django.db import models


# Cool story: однажды models.py разросся до 1к строк, и я сделал models package 😎

class User(AbstractUser):
    phone = models.CharField(max_length=33, blank=True)
    telegram_id = models.CharField(max_length=20, blank=True)
    telegram_username = models.CharField(max_length=70, blank=True)
