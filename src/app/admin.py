from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .models import User


@admin.register(User)
class AuthorAdmin(UserAdmin):
    def get_fieldsets(self, request, obj=None):
        return self.fieldsets + (
            (_("Custom fields"), {"fields": ("phone", "telegram_username")}),
        )
