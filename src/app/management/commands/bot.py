from django.core.management import BaseCommand

from app.internal.bot import start_bot


class Command(BaseCommand):
    help = 'Runs Telegram Bot'

    def handle(self, *args, **options):
        start_bot()
