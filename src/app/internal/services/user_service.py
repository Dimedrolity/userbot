import uuid
from typing import Tuple

import telegram

from app.models import User


def create_user(user: telegram.User):
    u = User(
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        telegram_username=user.username,
        telegram_id=user.id
    )
    u.save()


def get_user(telegram_user_id: str):
    return User.objects.get(telegram_id=telegram_user_id)


def user_exists(telegram_user_id) -> bool:
    return User.objects.filter(telegram_id=telegram_user_id).exists()


def is_user_registered(user: User) -> bool:
    return bool(user.phone)


def register_user(telegram_user_id: str, phone: str) -> Tuple[User, str]:
    """
    Registers user

    :param telegram_user_id: user Telegram ID
    :param phone: user phone number
    :return: user password
    """
    user = User.objects.get(telegram_id=telegram_user_id)
    user.phone = phone
    password = str(uuid.uuid4())
    user.set_password(password)
    user.save()

    return user, password
