import logging

import telegram
from django.conf import settings
from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes
from asgiref.sync import sync_to_async

from app.internal.services.user_service import create_user, user_exists, register_user, get_user, is_user_registered

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


def start_bot():
    application = ApplicationBuilder().token(settings.BOT_TOKEN).build()

    start_handler = CommandHandler('start', start)
    application.add_handler(start_handler)

    set_phone_handler = CommandHandler('set_phone', set_phone)
    application.add_handler(set_phone_handler)

    contact_handler = telegram.ext.MessageHandler(telegram.ext.filters.CONTACT, contact)
    application.add_handler(contact_handler)

    me_handler = CommandHandler('me', me)
    application.add_handler(me_handler)

    application.run_polling()


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    commands = (
        telegram.BotCommand('set_phone', 'Поделиться контактом'),
    )
    await context.bot.set_my_commands(commands, scope=telegram.BotCommandScopeChat(update.effective_chat.id))

    user = update.effective_user
    exists = await sync_to_async(user_exists)(user.id)
    if exists:
        await update.message.reply_html(
            f"О, я тебя знаю, ты {user.mention_html()}!",
        )
    else:
        await update.message.reply_html(
            f"Привет, {user.mention_html()}! Приятно познакомиться!",
        )
        await sync_to_async(create_user)(user)


async def set_phone(update: Update, _: ContextTypes.DEFAULT_TYPE):
    buttons = [
        [
            telegram.KeyboardButton('Запиши мой номерок!', request_contact=True),
        ],
    ]
    keyboard = telegram.ReplyKeyboardMarkup(buttons, one_time_keyboard=True)
    await update.message.reply_text(text="Может встретимся на выходных?", reply_markup=keyboard)


async def contact(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user, password = await sync_to_async(register_user)(update.effective_user.id, update.message.contact.phone_number)

    funny_sticker = 'CAACAgIAAxkBAAIBEmPtAhrTkvY1yM6BdshIcWAf9m2UAAL0EgAC9dC2HdU9YS6skDcTLgQ'
    await update.message.reply_sticker(funny_sticker)

    await update.message.reply_markdown_v2(text=f'Твой логин `{user.username}`\n' +
                                                f'Пароль `{password}`\n'
                                           .replace('.', '\\.')
                                           .replace('-', '\\-')
                                           )

    commands = (
        telegram.BotCommand('me', 'Обо мне'),
    )
    await context.bot.set_my_commands(commands, scope=telegram.BotCommandScopeChat(update.effective_chat.id))


async def me(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    user = await sync_to_async(get_user)(update.effective_user.id)
    if not user or not is_user_registered(user):
        await update.message.reply_text("Сначала /set_phone")
        return

    await update.message.reply_html(f"{update.effective_user.mention_html()}, {user.phone}")
