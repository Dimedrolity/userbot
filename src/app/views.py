from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse

from telegram.helpers import mention_html


def me(request):
    username = request.GET.get('username', '')
    password = request.GET.get('password', '')
    if 'username' not in request.GET and 'password' not in request.GET:
        return HttpResponseRedirect(redirect_to='?username=&password=')

    if not username and not password:
        return HttpResponse("<h3>Set username and password via query params.</h3>"
                            "<i>✨ It's magic ✨, params already in search bar, "
                            "just copy-paste values from Telegram Bot!</i>")

    user = authenticate(request, username=username, password=password)
    if user is not None:
        info = f'{mention_html(user.telegram_id, user.get_full_name())}, {user.phone}'
        return HttpResponse(info)
    else:
        return HttpResponse('invalid login')
